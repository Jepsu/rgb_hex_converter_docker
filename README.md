# RGB to Hex & Hex to RGB Converter

Welcome to the RGB to Hex & Hex to RGB Converter project! This project contains a web application for converting colors between RGB and Hex formats. The frontend is built using React, while the backend is developed with Node.js. The application is containerized using Docker, with the frontend and Nginx serving as one container and the backend in another. You can use Docker Compose to easily set up and run this project on a Linux VPS.

## Prerequisites

Before you get started, make sure you have the following software installed on your system:

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Getting Started

1. Clone the repository to your local machine:

 ```bash

 git clone https://gitlab.com/Jepsu/rgb_hex_converter_docker

 cd rgb_hex_converter_docker

 docker-compose build

 docker-compose up

 ```

The application should now be accessible at http://localhost:8080.

## Usage

Visit http://localhost:8080 in your web browser to access the RGB to Hex & Hex to RGB Converter web application.

## Project Structure

frontend/ contains the React frontend application.
backend/ contains the Node.js backend server.
docker-compose.yml defines the Docker Compose configuration for running both frontend and backend containers.
Contributing
Feel free to contribute to this project. You can submit issues, feature requests, or pull requests through our GitHub repository.


## Contact
If you have any questions or need further assistance, please feel free to contact us at Evgenii.Egorov@student.lab.fi.

Enjoy converting colors with RGB to Hex & Hex to RGB Converter!

