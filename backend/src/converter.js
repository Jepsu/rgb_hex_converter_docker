
function ConvertRGBtoHex(red, green, blue) {
  if (
    Number.isInteger(red) && Number.isInteger(green) && Number.isInteger(blue) &&
    red >= 0 && red <= 255 && green >= 0 && green <= 255 && blue >= 0 && blue <= 255
  ) {
    const hex = `#${(1 << 24 | red << 16 | green << 8 | blue).toString(16).slice(1)}`;
    
    // Log the HEX value to the console
    console.log('HEX value inside converter file:', hex);

    return hex;
  } else {
    throw new Error('Invalid RGB values. Each value should be an integer between 0 and 255.');
  }
}
function hexToRgb(hex) {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    }

module.exports = {


  ConvertRGBtoHex:ConvertRGBtoHex,
  hexToRgb:hexToRgb
  
  }
  